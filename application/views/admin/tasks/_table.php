<div class="row" style="margin-bottom: 6%; margin-left: 0%">
<div class="col-md-12" >

    <ul class="dropdown-menu custom_drop_menu show">
        <li class="label label-info custom_datatable_task">
            <a class="" href="#" data-cview="task_type_call" onclick="dt_custom_view('task_type_call','.table-tasks','task_type_call'); return false;">
                   <i class="fa fa-phone" aria-hidden="true"></i>Call
            </a>
        </li>
        <li class="label label-info custom_datatable_task">
            <a class="" href="#" data-cview="task_type_work" onclick="dt_custom_view('task_type_work','.table-tasks','task_type_work'); return false;">
                 <i class="fa fa-briefcase" aria-hidden="true"></i> Work
            </a>
        </li>
        <li class="label label-info custom_datatable_task">
            <a href="#" data-cview="task_type_meeting" onclick="dt_custom_view('task_type_meeting','.table-tasks','task_type_meeting'); return false;">
                    <i class="fa fa-handshake-o" aria-hidden="true"></i> Meeting
            </a>
        </li>
        <li class="label label-info custom_datatable_task">
            <a href="#" data-cview="task_type_sendquote" onclick="dt_custom_view('task_type_sendquote','.table-tasks','task_type_sendquote'); return false;">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> Send Quote
            </a>
        </li>

    </ul>
</div>

<div id="tasks_agents_custom_date_range" class="hide col-md-4">
        <div class="row">
            <div class="col-md-6">
                <label for="report-from" class="control-label"><?php echo _l('report_sales_from_date'); ?></label>
                <div class="input-group date">
                    <input type="text" class="form-control datepicker" id="tasks-from-agents" name="tasks-from-agents">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar calendar-icon"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label for="report-to" class="control-label"><?php echo _l('report_sales_to_date'); ?></label>
                <div class="input-group date">
                    <input type="text" class="form-control datepicker" id="tasks-to-agents" name="tasks-to-agents">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar calendar-icon"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php

$table_data = array(
    _l('tasks_dt_type'),
    _l('tasks_dt_name'),
    _l('tasks_dt_datestart_action'),
    _l('task_duedate_action'),
    _l('tags'),
    _l('task_assigned'),
    _l('tasks_list_priority'),
    _l('task_status')
);

if (isset($bulk_actions)) {
    array_unshift($table_data, '<span class="hide"> - </span><div class="checkbox mass_select_all_wrap"><input type="checkbox" id="mass_select_all" data-to-table="tasks"><label></label></div>');
}

$custom_fields = get_custom_fields('tasks', array(
    'show_on_table' => 1
));

foreach ($custom_fields as $field) {
    array_push($table_data, $field['name']);
}

$table_data = do_action('tasks_table_columns', $table_data);

array_push($table_data, _l('options'));

render_datatable($table_data, 'tasks');
?>

<style>
    .custom_datatable_task{
        font-size: 18px;
        text-transform: capitalize;
        margin-right: 10px;
    }
    .custom_bg_button{
        color: white;
        background-color: #03a9f4;
    }
    .custom_drop_menu{
        z-index: 0;
        border: none;
        box-shadow: none;
    }
    .custom_drop_menu li{
        padding: 0px;
        float: left;
    }
    .custom_drop_menu li a{
        display: inline;
        color: #03a9f4;

        border-radius: 0px !important;
        padding-top: 0px;
        padding-bottom: 0px;
    }
</style>