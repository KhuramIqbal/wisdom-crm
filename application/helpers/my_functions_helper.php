<?php
add_action('after_render_single_aside_menu', 'my_custom_menu_items');

function my_custom_menu_items($order)
{
    if ($order == 2) {
        echo '<li>';
        echo '    <a href="' . base_url() . 'admin/invoices/invoice_renewels_items" aria-expanded="false"><i class="fa fa-registered menu-icon"></i>Renewals</a>';
        echo '</li>';
    }

    if ($order == 3) {

        echo '<li>';
        echo '    <a href="#" aria-expanded="false"><i class="fa fa-registered menu-icon"></i>Receipts<span class="fa arrow"></span></a>';
        echo '    <ul class="nav nav-second-level collapse" aria-expanded="false">';


        if (is_admin() || has_permission('receipts', '', 'view') || has_permission('receipts', '', 'view_own')) {
            echo '        <li><a href="' . base_url() . 'admin/receipts/">All Receipts</a></li>';
        }


        if (is_admin() || has_permission('receipts', '', 'create')) {
            echo '        <li><a href="' . base_url() . 'admin/receipts/create">Create New</a></li>';
        }

        if (is_admin() || has_permission('receipt_handover', '', 'edit')) {
            echo '        <li><a href="' . base_url() . 'admin/receipts/index/handover">Handover</a></li>';

        }

        if (is_admin() || has_permission('receipt_deposit', '', 'edit')) {
            echo '        <li><a href="' . base_url() . 'admin/receipts/index/deposited">Deposit</a></li>';
        }

        if (is_admin() || has_permission('receipt_verify', '', 'edit')) {
            echo '        <li><a href="' . base_url() . 'admin/receipts/index/verified">Verify</a></li>';
        }

        echo '    </ul>';
        echo '</li>';

    }

    if ($order == 7) {

        echo '<li>';
        echo '    <a href="#" aria-expanded="false"><i class="fa fa-file-text-o menu-icon"></i>Custom Notes<span class="fa arrow"></span></a>';
        echo '    <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li><a href="' . base_url() . 'admin/custom_notes/">View All</a></li> 
                    <li><a href="' . base_url() . 'admin/custom_notes/create">Create New</a></li> 
                  </ul>';
        echo '</li>';

    }
}